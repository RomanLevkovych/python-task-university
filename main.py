from Book import Book
from Books import Books
from pprint import pprint
from constants import DATA_FILE
import csv

def menu():
    print("""
            |Enter respective number with parameter to apply a function
            |0. print menu
            |1. print file
            |2. search by author's surname
            |3. sort by key
            |4. remove
            |5. add element
            |6. edit by index of element
            |7. exit""")

if __name__ == '__main__':
    books = []
    with open(DATA_FILE) as data:
        data = csv.reader(data)
        for item in data:
            books.append(Book(*item))
        books = Books(books)
        while True:
            menu()
            section = int(input())
            if 0 == section:
                menu()
            elif 1 == section:
                pprint(books)
            if 2 == section:
                surname = input()
                pprint(books.search(surname))
            elif 3 == section:
                key = input()
                if key == "name":
                    books.sort(key=lambda x: x.name)
                elif key == "author":
                    books.sort(key=lambda x: x.author)
                elif key == "pages":
                    books.sort(key=lambda x: x.pages)
                elif key == 'publisher':
                    books.sort(key=lambda x: x.publisher)
                elif key == 'year':
                    books.sort(key=lambda x: x.year)
                pprint(books)
            elif 4 == section:
                index = int(input("Please enter index element to remove"))
                books.pop(index)
            elif 5 == section:
                print("adding new element")
                book = Book()
                book.name = input("Enter name of book: ")
                book.author = input("Enter author of book: ")
                book.pages = int(input("Enter number of pages: "))
                book.publisher = input("Enter publisher of books: ")
                book.year = int(input("Enter year of publishing: "))
                books.append(book)
                pprint(books)
            elif 6 == section:
               index = int(input("Enter index: "))
               books.edit(index)
               pprint(books)
            elif 7 == section:
                print("Bye for now")
                break