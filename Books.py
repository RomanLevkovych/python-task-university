from Book import Book
from constants import DATA_FILE
from pprint import pprint
import csv

class Books:
    def __init__(self, books: [Book]=list()):
        self._books = books

    @property
    def books(self):
        '''return list of books'''
        return self._books

    def append(self, book: Book):
        '''add book to object'''
        self._books.append(book)
        self.update_file(book)

    def pop(self, index: int):
        ''' pop value from Books'''
        value_to_remove = self._books.pop(index)
        self.rewrite_file()
        return value_to_remove

    def sort(self, key=lambda x: x.name, reverse: bool=False):
        '''sort books'''
        for i in range(len(self.books)):
            for j in range(len(self.books)):
                value_i = key(self._books[i])
                value_j = key(self._books[j])
                if type(value_i) is str:
                    value_i = value_i.lower()
                    value_j = value_j.lower()
                if reverse:
                    if value_i > value_j:
                        self._books[i], self._books[j] = self._books[j], self._books[i]
                else:
                    if value_i < value_j:
                        self._books[i], self._books[j] = self._books[j], self._books[i]
        self.rewrite_file()

    def search(self, value ) -> [Book]:
        '''search for element in books'''
        res = list()
        if type(value) is str:
            value = value.lower()
        for i in range(len(self.books)):
            attributes = self.books[i].__dict__.values()
            for attr in attributes:
                if type(attr) is str:
                    attr = attr.lower()
                if value in attr:
                    res.append((i, self.books[i]))
                    continue
        return res

    def __str__(self):
        string = ''
        for i in range(len(self.books)):
            string += str(i) + " " + str(self.books[i]) + '\n'
        return string

    def __repr__(self):
        string = ''
        for i in range(len(self.books)):
            string += str(i) + " " + str(self.books[i]) + '\n'
        return string    

    def update_file(self, value):
        '''update data in file after changes'''
        file = open(DATA_FILE, 'a+')
        data = csv.writer(file)
        data.writerow(value.__dict__.values())
        file.close()

    def rewrite_file(self):
        '''erases file and fill it with actual data'''
        file = open(DATA_FILE, 'w')
        data = csv.writer(file)
        for book in self._books:
            data.writerow(book.__dict__.values())
        file.close()   

    def read_file(self):
        '''read data from file'''
        data = open(DATA_FILE)
        data = csv.reader(data)
        for item in data:
            self._books.append(Book(*item))
        data.close()

    def edit(self, index: int):
        if index < len(self.books) and index >= 0: 
            self._books[index].edit()
            self.rewrite_file()
        else:
            print("Error")

if __name__ == '__main__':
    books = []
    with open('data.csv') as data:
        data = csv.reader(data)
        for item in data:
            books.append(Book(*item))
    books = Books(books)
    pprint(books)
    print("\n\n")
    books.sort(key=lambda x: x.author, reverse=True)
    pprint(books)
    print("\n\n")
    pprint(books.search("250"))