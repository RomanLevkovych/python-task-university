import csv
import constants

class Book:   
    def __init__(self, name='', author='', pages=0, publisher='', year=0):
        self._name = name
        self._author = author
        self._pages = pages
        self._publisher = publisher
        self._year = year

    @property
    def name(self):
        return self._name

    @property
    def author(self):
        return self._author

    @property
    def pages(self):
        return self._pages

    @property
    def publisher(self):
        return self._publisher

    @property
    def year(self):
        return self._year

    @name.setter
    def name(self, value):
        check = True
        counter = 1 
        while check:
            if counter > 1:
                    value = input()
            if type(value) is str:
                self._name = value
                check = False
            else:
                print("Please, enter correct name")
                counter += 1
    
    @author.setter
    def author(self, value):
        check = True
        counter = 1
        while check:
            if counter > 1:
                value = input()
            if type(value) is str and all(map(str.isalpha, value.split())):
                self._author = value  
                check = False
            else:
                print("Please enter correct author name")
                counter += 1

    @pages.setter
    def pages(self, value):
        check = True
        counter = 1
        while check:
            if counter > 1:
                value = input()
            if type(value) is int and value > 0:
                self._pages = value
                check = False
            else:
                print("Plaese, enter correct number of pages", end=" ")
                counter += 1

    @publisher.setter
    def publisher(self, value):
        check = True
        counter = 1
        while check:
            if counter > 1:
                value = input()
            if type(value) is str and all(map(str.isalpha, value.split())):
                self._publisher = value
                check = False 
            else:
                print("Enter correct name of publisher")
                counter += 1
            

    @year.setter
    def year(self, value):
        check = True
        counter = 1
        while check:
            if counter > 1:
                value = int(input())
            if type(value) is int and value >= 0 and value <=constants.CURRENT_YEAR:
                self._year = value  
                check = False
            else:
                print("Enter correct year of publishing")
                counter += 1

    def edit(self):
        to_edit = "asdf"
        while to_edit != "exit":
            to_edit = input("Enter field to edit(name, author, number of pages, publisher, year of publishing or exit): ")
            if "name" == to_edit:
                self.name = input("Enter new name: ")
            elif "author" == to_edit:
                self.author = input("Enter new author of the book: ")
            elif "number of pages" == to_edit:
                self.pages == int(input("enter new number of pages"))
            elif "publisher" == to_edit:
                self.publisher = input("Enter new publisher")
            elif "year of publishing" == to_edit:
                self.year = int(input())

    def __str__(self):
        return "{0} by {1} ({2} pages. {3} ({4}))".format(self.name, self.author, self.pages, self.publisher, self.year)
    
    def __repr__(self):
        return "Book(name={0}, author={1}, pages={2}, publisher={3}, year={4})".format(self.name, self.author, self.pages, self.publisher, self.year)

# if __name__ == '__main__':
#     with open("data.csv") as data:
#         input_data = csv.reader(data)
#         for book in input_data:
#             print(Book(*book))